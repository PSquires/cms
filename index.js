/**
 * Prog8020 Project - Content Management System
 * Paul Squires
 */


//---------------- Setup ------------------

const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const { check, validationResult } = require('express-validator');
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/cms', {
    useNewUrlParser: true
});
const session = require('express-session');
const fileUpload = require('express-fileupload');

var myApp = express();

myApp.use(bodyParser.urlencoded({ extended: false }));
myApp.use(bodyParser.json())
myApp.use(session(
    { secret: 'changeMe', resave: false, saveUninitialized: true }
));
myApp.use(fileUpload());
myApp.set('views', path.join(__dirname, 'views'));
myApp.use(express.static(__dirname + '/public'));
myApp.set('view engine', 'ejs');


//---------------- Classes ------------------
const PageData = mongoose.model('Page', {
    url: String,
    title: String,
    html: String,
    image: String
});


const HeaderData = mongoose.model('Header', {
    name: String,
    title: String,
    tagline: String,
    footer: String,
    image: String
});

const Admin = mongoose.model('Admin', {
    username: String,
    password: String
});

class PageOutput {
    constructor() {
        this.headerData = null;
        this.pageData = null;
        this.pageList = null;
        this.errors = [""];
    }

    addError(error) {
        this.errors.push(error);
    }
};

//------------------ Other initialization ------------------

console.log('Initializing...');
var pageList;
PageData.find({}, function (err, pages) {
    if (err) { console.log(err); }
    pageList = pages;
    // console.log(contacts);
    // console.log(pages);
});

Admin.find({}, function (err, admins) {
    if (err) { console.log(err); }
    console.log(admins);
});

var headerData;
HeaderData.findOne({}, function (err, header) {

    headerData = header;
});

var urlRegex = /^[\w-_~]+[\w-_~.]*$/;

function CreatePageOutput(pageData = null) {
    var pageOutput = new PageOutput();
    pageOutput.pageData = pageData;
    pageOutput.headerData = headerData;
    pageOutput.pageList = pageList;

    return pageOutput;
}

//---------------- Routes ------------------

myApp.get('/', function (req, res) {
    res.redirect('/home');
});

myApp.get('/admin/add', function (req, res) {
    if (req.session.userLoggedIn) {
        res.render('createpage', { output: CreatePageOutput() });
    }
    else {
        res.redirect('/admin/login');
    }
});

myApp.post('/admin/add', [
    check('title', 'Please enter the title').not().isEmpty(),
    check('url', 'URL is not in correct format').matches(urlRegex),
], function (req, res) {
    if (!req.session.userLoggedIn) {
        res.redirect('/admin/login');
        return;
    }
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        var output = CreatePageOutput();
        output.errors = errors.array();
        res.render('createpage', { output: output });
    }
    else {
        var title = req.body.title;
        var url = req.body.url.toLowerCase();
        var message = req.body.message;
        var imageName = "";
        if (req.files != null && req.files.imageUpload != null) {
            imageName = req.files.imageUpload.name;
            var image = req.files.imageUpload;
            var imagePath = 'public/images/' + imageName;
            image.mv(imagePath, function (err) {
                if (err) { console.log('Could not save image'); }
            });
        }

        var page = new PageData({
            url: url,
            title: title,
            html: message,
            image: imageName
        });
        page.save().then(() => {
            console.log('New page created: ' + page.url);
        });
        res.redirect('/admin/manage');
    }
});


// ------------ New Routes ---------------------

myApp.get('/admin', function (req, res) {
    if (req.session.userLoggedIn) {
        res.redirect('/admin/manage');
    }
    else {
        res.redirect('/admin/login');
    }
});

myApp.get('/admin/login', function (req, res) {
    if (req.session.userLoggedIn) {
        res.redirect('/admin/manage');
    }
    else {
        res.render('login', { output: CreatePageOutput() });
    }
});

myApp.post('/admin/login', function (req, res) {
    var username = req.body.username;
    var password = req.body.password;

    console.log("Username: " + username);
    console.log("Password: " + password);
    console.log(req.body);

    Admin.findOne({ username: username, password: password }, function (err, admin) {
        if (err || admin == null) {
            var output = CreatePageOutput();
            output.addError("Invalid Login");
            res.render('login', { output: output });
            console.log(output);
        }
        else {
            req.session.username = admin.username;
            req.session.userLoggedIn = true;
            res.redirect('manage');
        }
    });
});

myApp.get('/admin/logout', function (req, res) {
    req.session.destroy();
    res.render('logout', { output: CreatePageOutput() });
});

myApp.get('/admin/manage', function (req, res) {
    if (req.session.userLoggedIn) {
        PageData.find({}, function (err, pages) {
            if (err) { throw err; }
            pageList = pages;
            var output = CreatePageOutput();
            res.render('manage', { output: output });
        });
    }
    else {
        res.redirect('login');
    }
});

myApp.get('/admin/edit/:id', function (req, res) {
    if (req.session.userLoggedIn) {
        var url = req.params.id;
        PageData.findOne({ url: url }, function (err, page) {
            if (err || page == null) {
                console.log('Page not found: ' + url);
                res.render('notfound', { output: CreatePageOutput() });
            }
            else {
                res.render('editpage', { output: CreatePageOutput(page) });
            }
        });
    }
    else {
        res.redirect('/admin/login');
    }
});

myApp.post('/admin/edit/:id', [
    check('title', 'Please enter the title').not().isEmpty(),
    check('url', 'URL is not in correct format').matches(urlRegex),
], function (req, res) {
    if (req.session.userLoggedIn) {
        var url = req.params.id;

        const errors = validationResult(req);


        PageData.findOne({ url: url }, function (err, page) {
            if (err || page == null) {
                console.log(err);
                console.log('Page not found: ' + url);
                var output = CreatePageOutput();
                output.addError('Page: ' + url + ' not found');
                res.render('notfound', { output: output });
            }
            else if (!errors.isEmpty()) {
                var output = CreatePageOutput(page);
                output.errors = errors.array();
                res.render('editpage', { output: output });
            }
            else {
                page.title = req.body.title;
                page.url = req.body.url.toLowerCase();
                page.html = req.body.message;
                var imageName = "";
                if (req.files != null && req.files.imageUpload != null) {
                    var imageName = req.files.imageUpload.name;
                    var image = req.files.imageUpload;
                    var imagePath = 'public/images/' + imageName;
                    image.mv(imagePath, function (err) {
                        if (err) { console.log(err); }
                    });
                }
                page.image = imageName;
                page.save().then(() => {
                    console.log('Updated: ' + url);
                });
                res.redirect('/admin/manage');
            }
        });

    }
    else {
        res.redirect('/admin/login');
    }
});


myApp.get('/admin/editheader', function (req, res) {
    if (req.session.userLoggedIn) {
        if (headerData != null) {
            var output = CreatePageOutput();
            res.render('editheader', { output: output });
        }
        else {
            console.log('No Header Data found');
            throw (new Error('No Header Data found'));
        }
    }
    else {
        res.redirect('/admin/login');
    }
});

myApp.post('/admin/editheader',
    [check('title', 'Please enter a title').not().isEmpty()],
    function (req, res) {

        if (req.session.userLoggedIn) {
            const errors = validationResult(req);
            if (errors.isEmpty()) {

                if (headerData != null) {
                    headerData.title = req.body.title;
                    headerData.tagline = req.body.tagline;
                    headerData.footer = req.body.footer;
                    var imageName = "";
                    if (req.files != null && req.files.imageUpload != null) {
                        imageName = req.files.imageUpload.name;
                        var image = req.files.imageUpload;
                        var imagePath = 'public/images/' + imageName;
                        image.mv(imagePath, function (err) {
                            if (err) { console.log(err); }
                        });
                    }
                    headerData.image = imageName;
                    headerData.save();

                    console.log('Header Saved');

                    res.redirect('manage');
                }
                else {
                    console.log('No Header Data found');
                    throw (new Error('No Header Data found'));
                }
            }
            else {
                var output = CreatePageOutput();
                output.errors = errors.array();
                console.log(errors.array());
                res.render('editheader', { output: output })
            }
        }
        else {
            res.redirect('/admin/login');
        }
    });

myApp.get('/admin/delete/:id', function (req, res) {
    if (req.session.userLoggedIn) {
        var url = req.params.id;
        PageData.findOneAndRemove({ url: url }, function (err, page) {
            if (err || page == null) {
                var output = CreatePageOutput();
                output.addError("Page not found!");
                res.redirect('/admin/manage');
                console.log(err);

                //throw err;
            }
            else {
                console.log("Deleted " + page.url);
                res.redirect('/admin/manage');
            }
        });
    }
    else {
        res.redirect('/admin/login')
    }
});

// Will accept any url and try to display it
myApp.get('/:id', function (req, res) {
    var url = req.params.id;
    console.log(url);
    PageData.findOne({ url: url }, function (err, pageData) {
        if (err || pageData == null) {
            console.log(err);
            res.render('notfound', { output: CreatePageOutput() });
        }
        else {
            res.render('page_template', { output: CreatePageOutput(pageData) });
        }
    });
});

//----------- Start the server -------------------

myApp.listen(8080);
console.log('Server started at 8080 for mywebsite...');

