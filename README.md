# CMS
Paul Squires

A content management system that allows users to login and create, edit, or delete pages on a website.
_____

Startup - use NPM and Node.js
cd to the root directory of this project (the same folder as index.js)

Install dependencies:
`npm install`

Then, to run:
`node index.js`

_____

**License**

http://www.gnu.org/licenses/gpl-3.0.html
General Public Use license.

Allows anyone to use or add to this code and redistribute it.
This project is meant to be open-source, and this allows people to modify to the code and use it with the same license.
It also protects the authors rights to this software, but anyone modifying it will have the same freedoms.